﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		//This is some text we don't want to have happen
		//transform.position += Vector3.right;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.position += Vector3.right*Time.deltaTime;
	}
}
